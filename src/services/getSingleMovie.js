export default function getSingleMovie(id) {
  return fetch(`https://mcuapi.herokuapp.com/api/v1/movies/${id}`)
    .then((res) => res.json())
    .then((json) => {
      const {
        chronology,
        cover_url,
        directed_by,
        duration,
        id,
        overview,
        phase,
        release_date,
        saga,
        title,
        trailer_url,
      } = json

      const trailerWithHttps = trailer_url ? trailer_url.replace('http://', 'https://') : null
      const coverWithHttps = cover_url ? cover_url.replace('http://', 'https://') : null

      return {
        chronology,
        coverWithHttps,
        directed_by,
        duration,
        id,
        overview,
        phase,
        release_date,
        saga,
        title,
        trailerWithHttps,
      }
    })
}
