export default function getAllMovies() {
  return fetch('https://mcuapi.herokuapp.com/api/v1/movies')
    .then((res) => res.json())
    .then((json) => {
      const { data } = json

      return data
    })
}
